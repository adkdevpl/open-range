import { IItem } from 'app/shared/model//item.model';

export interface IStock {
    id?: number;
    quantity?: number;
    price?: string;
    item?: IItem;
}

export class Stock implements IStock {
    constructor(public id?: number, public quantity?: number, public price?: string, public item?: IItem) {}
}
