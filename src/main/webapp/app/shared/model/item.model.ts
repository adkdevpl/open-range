export interface IItem {
    id?: number;
    sku?: string;
    name?: string;
}

export class Item implements IItem {
    constructor(public id?: number, public sku?: string, public name?: string) {}
}
