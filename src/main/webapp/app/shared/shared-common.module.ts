import { NgModule } from '@angular/core';

import { OrangeSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [OrangeSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [OrangeSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class OrangeSharedCommonModule {}
