import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IStock } from 'app/shared/model/stock.model';
import { Principal } from 'app/core';
import { StockService } from './stock.service';

@Component({
    selector: 'jhi-stock',
    templateUrl: './stock.component.html'
})
export class StockComponent implements OnInit, OnDestroy {
    stocks: IStock[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private stockService: StockService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.stockService.query().subscribe(
            (res: HttpResponse<IStock[]>) => {
                this.stocks = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInStocks();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IStock) {
        return item.id;
    }

    registerChangeInStocks() {
        this.eventSubscriber = this.eventManager.subscribe('stockListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
