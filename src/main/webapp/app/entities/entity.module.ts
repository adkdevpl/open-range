import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { OrangeItemModule } from './item/item.module';
import { OrangeStockModule } from './stock/stock.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        OrangeItemModule,
        OrangeStockModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrangeEntityModule {}
