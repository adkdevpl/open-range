/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.adkdev.web.rest.vm;
